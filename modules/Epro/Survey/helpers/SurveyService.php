<?php
namespace App\Epro\Modules\Survey\Helpers;

use App\Epro\Modules\Config\Models\Config as ConfigModel;

class SurveyService{

	private $apiUrl;

	private $username;

	private $password;

	private $sessionKey = null;

	private $client = null;

	public function __construct()
	{
		try
		{
			$config = ConfigModel::all()->keyBy('key')->lists('value','key');
			$this->apiUrl = $config['api_url'];
			$this->username = $config['api_login'];
			$this->password = $config['api_password'];

			// instanciate a new client
			$myJSONRPCClient = $this->client = new \org\jsonrpcphp\JsonRPCClient( $this->apiUrl.'/admin/remotecontrol' );

			// receive session key
			$this->sessionKey= $myJSONRPCClient->get_session_key( $this->username, $this->password );

		}
		catch(\Exception $e)
		{

		}
	}

	public function checkClient()
	{
		if(!$this->sessionKey)
		{
			return false;
		}
		return true;
	}

	public function getSurveys()
	{
		if(!$this->checkClient())
		{
			return null;
		}
		try
		{
			return $this->client->list_surveys( $this->sessionKey );
		}
		catch(\Exception $e)
		{

		}
	}

	public function getStat($surveyID, $type)
	{
		try
		{
			if(!$this->client)
			{
				return null;
			}
			$output = $this->client-> export_statistics( $this->sessionKey, $surveyID , $type);
			if($type == "html")
			{
				$html = base64_decode($output);
				$html = preg_replace('/statisticstable/','table table-hover', $html);
				$html = preg_replace('/statisticssummary/','table table-hover', $html);

				return $html;
			}
			elseif($type == "xls")
			{
				$filename = "limesurvey_data.xls";
				file_put_contents('uploads/'.$filename, base64_decode($output));
				echo json_encode(array(
					'file' => '/uploads/'.$filename,
				));
				return ;
			}
			elseif($type == "pdf")
			{
				$filename = "limesurvey_data.pdf";
				file_put_contents('uploads/'.$filename, base64_decode($output));
				echo json_encode(array(
					'file' => '/uploads/'.$filename,
				));
				return ;
			}
		}
		catch(\Exception $e)
		{

		}
		
	}

	/**
	 * get survey asnwears
	 */


	public function getSurveyAnswers($surveyID, $type='simple')
	{
		if($this->getSummary($surveyID) == 0)
		{
			return [
				'error' => false,
				'content' => 'brak danych'
			];
		}
		if($type == "full")
		{

			$base64 = $this->client->export_responses( $this->sessionKey, $surveyID, 'json', null, 'all','full','long',null,null,null);

		}
		else
		{
			$base64 = $this->client->export_responses( $this->sessionKey, $surveyID, 'json');
		}
		

		if(is_array($base64))
		{
			return [
					'error' => 'true',
					'message' => $base64['status']
				];
		}

		return [
			'error' => false,
			'content' => json_decode(base64_decode($base64))
		];
	}

	/**
	 * get question asnwears
	 */
	public function getQuestions($surveyID)
	{


		$questions = $this->client->list_questions( $this->sessionKey, $surveyID);
		return $questions;
	}
	public function getSingleAnswers($surveyID,$token,$type)
	{

		$base64 = $this->client->export_responses( $this->sessionKey, $surveyID, 'json', null, 'all','$type','long',null,null,null);

		$questions=json_decode(base64_decode($base64));
		return $questions;
	}

	public function getSingleAnswer($surveyID,$token)
	{

		$base64 = $this->client->export_responses_by_token( $this->sessionKey, $surveyID, 'json', $token, '','complete','full','long');
		
		$questions=json_decode(base64_decode($base64));

		return $questions;
	}

	public function getSummary($survey_id)
	{
		return $this->client->get_summary($this->sessionKey, (int)$survey_id, 'completed_responses');
	}
}
