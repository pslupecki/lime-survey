
<div class="footer">
    Strona <span class="pagenum"></span>
</div>
	<table class="table table-hover subtable new_table">
	<tbody class="new_table"  width="595px;" >
		<tr  style="width:400px !important;">
			<th width="30%">{{ trans('surveyTranslate::list.question') }}</th>
			<th width="70%" class="text-left">{{ trans('surveyTranslate::list.answer') }}</th>
		</tr>

			@foreach($answers as $key=>$ans)
		<tr  style="width:100px !important;">
			<td  style="padding-top:5px; padding-bottom:3px; width:400px !important;" >{{$key}}</td>
			<td  style="white-space:pre-wrap; padding-top:3px; padding-bottom:3px; letter-spacing:1px; width:400px !important;">
				{{$ans}}
			</td>
		</tr>
		@endforeach
		</tbody>
	</table>

	@if(isset($notes))

	<table class="table-hover subtable" style="border-style:outset;  border-width: 1px;}"><h1>Tabela oceny ankiety</h1>
		<tr style="width:400px !important;">
			<th width="5%">#</th>
			<th width="35%">{{ trans('surveyTranslate::list.question') }}</th>
			<th width="60%" class="text-left">{{ "Ocena" }}</th>
		</tr>
		{{--*/ $it = 1; /*--}}
		{{--*/ $count = 0; /*--}}
		@foreach ($notes as $key=>$note)
			<tr style="width:100px !important;">
			@if($it>8)
				{{--*/ $count ++; /*--}}
				<td class="" style="border-style:outset;  border-width: 1px; padding-left:20px;"><?=$count;?></td>
				<td class="" style="border-style:outset;  border-width: 1px; white-space:pre-wrap; "><?=$key;?></td>
				<td class="" style="border-style:outset;  border-width: 1px; white-space:pre-wrap;
				padding-top:2px; padding-bottom:2px; letter-spacing:0px;
				">
			 <?php if(is_numeric($note))
			 	 { 
			 	 	echo intval($note); 
			 	 }
			 	  else { 
			 	  	echo $note;
			 	  }
			 ?>
			</td>
			@endif
		</tr>
		{{--*/ $it ++; /*--}}
		@endforeach
	</table>
	@endif
	@if(isset($notes2) && count((array)$notes2))

	<table class="table-hover subtable" style="border-style:outset;  border-width: 1px;}"><h1>Tabela oceny ankiety nr 2</h1>
		<tr style="width:500px !important;">
			<th width="5%">#</th>
			<th width="35%">{{ trans('surveyTranslate::list.question') }}</th>
			<th width="60%" class="text-left">{{ "Ocena" }}</th>
		</tr>
		{{--*/ $it = 1; /*--}}
		{{--*/ $count = 0; /*--}}
		@foreach ($notes2 as $key=>$note)
		<tr style="width:100px !important;">
		 	@if($it>8)
			{{--*/ $count ++; /*--}}
				<td class="" style="border-style:outset;  border-width: 1px; padding-left:20px;"><?=$count;?></td>
				<td class="" style="border-style:outset;  border-width: 1px; white-space:pre-wrap; "><?=$key;?></td>
				<td class="" style="border-style:outset;  border-width: 1px; white-space:pre-wrap;
				padding-top:2px; padding-bottom:2px; letter-spacing:0px;
				">
				 <?php if(is_numeric($note)) {echo intval($note); }
				 	  else { echo $note;}
				 ?>
			</td>
			@endif
		</tr>
		{{--*/ $it ++; /*--}}
	@endforeach
	@endif
	</table>
</div>
