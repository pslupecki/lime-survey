@extends('_layouts.default')
@section('breadcrumb')
	@parent {{ trans('headers.separator') }} {{ trans('surveyTranslate::form.header') }}
@stop

@section('main')

<div class="row">
	<div class="box clearfix">
	    <div class="header">
			<h3 class="text-center">
				{{trans('surveyTranslate::form.header')}}
			</h3>
		</div>
		<div class="col-xs-12 col-sm-12">
			<div class="row">
				<div class="form-group">
					@if(is_array($questions) && count($questions) > 0 && !isset($questions['status']))
					<table class="table table-hover">
						<tr>
							<th class="">#</th>
							<th class="">{{ trans('surveyTranslate::list.name') }}</th>
							<th class="text-center">{{ trans('surveyTranslate::list.active') }}</th>
							<th class="text-center">{{ trans('surveyTranslate::list.actions') }}</th>
						</tr>
					@if(is_array($questions) && count($questions) > 0)
						@foreach ($questions as $key=>$question)
							<tr>
								<td class="">{{ $key=1 }}</td>
								<td class="">{{ $question['surveyls_title'] }}</td>
								<td class="text-center">
								@if($question['active'] == "Y")
									<span class="btn btn-xs btn-success"><i class="fa fa-check-circle"></i></span>
								@else
									<span class="btn btn-xs btn-danger"><i class="fi-alert"></i></span>
								@endif
								</td>
								<td class="text-center">

								@if($question['active'] == "Y")
									<a href="{{ URL::route('survey.surveyanswers', array('sid' => $question['sid'])) }}" class="btn btn-xs btn-default">Odpowiedzi</a>
									@if(in_array(8,SESSION::get('acl_roles')))
									@if($question['exp']=="")
									<a href="{{ URL::route('survey.process', array('sid' => $question['sid'])) }}" class="btn btn-xs btn-default">Przydziel eksperta</a>
									@endif

									<a href="{{ URL::route('survey.htmlstat', array('sid' => $question['sid'])) }}" class="btn btn-xs btn-info">HTML</a>
									<a href="#" data-type="pdf" data-sid="{{ $question['sid'] }}" class="survery-stat btn btn-xs btn-danger">PDF</a>
									<a href="#" data-type="xls" data-sid="{{ $question['sid'] }}" class="survery-stat btn btn-xs btn-success">XLS</a>
									@endif
								@endif
								</td>
							</tr>
						@endforeach
					@endif
					</table>
					@elseif(isset($questons['status']))
						{{ $questions['status'] }}
					@endif
					<a id="download" href="pdfData" target="_blank" title='Download pdf document' class="hidden">DOWNLOAD</a>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer_scripts')
<script type="text/javascript">
$(document).ready(function(){
	$('.survery-stat').click(function(){
		var type = $(this).data('type');
		$.ajax({
			dataType: 'json',
			url: '/epro/panel/public/survey/stat/'+$(this).data('sid')+'/'+type,
			success: function(respond){
				document.getElementById("download").href = respond.file;
	        	document.getElementById("download").click();
			},
		})
	})
});
</script>
@stop
