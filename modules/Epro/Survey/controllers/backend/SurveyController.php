<?php
namespace App\Epro\Modules\Survey\Controller\Backend;

use App;
use App\Epro\Modules\Acl\Helpers\AclHelper;
use DB;
use Illuminate\Support\Facades\Auth;
use Input;
use Notification;
use Redirect;
use Request;
use Session;
use User;
use Validator;
use View;
use \App\Epro\Modules\Survey\Helpers\SurveyService;
use App\Epro\Modules\Client\Models\Client as ClientModel;
use App\Epro\Modules\Client\Models\ClientRole as ClientRoleModel;
use App\Epro\Modules\Survey\Models\SurveyAnswerRelation as SurveyAnswerModel;
use Schema;
use Paginator;
use \stdClass;
use \SimpleXMLElement;
include(app_path().'/libs/autoload.inc.php');
use Dompdf\Dompdf;
use Dompdf\Options;


class SurveyController extends \BaseController{

    /**
     * Set user data
     */
    public function __construct() {
    	$this->user = Auth::getUser();
    	if (!is_object($this->user)) {
    		App::abort(403, 'Nie masz dostępu do tej części serwisu. Musisz się zalogować');
    	}
    	AclHelper::Auth('survey');
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		
		$user=Sentry::user();
		$LSmanager = new SurveyService();

		$content = $LSmanager->getSurveys();

		return View::make('surveyTheme::backend.clientsurvey.index')
			->with('content',$content)
			->with('user',$userid);
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function stat($surveyID,$type)
	{
		$LSmanager = new SurveyService();

		$questions = $LSmanager->getStat($surveyID, $type);

		return $questions;
	}
	public function pdf($surveyID,$answerID)
	{


		$LSmanager = new SurveyService();
		$surveys = $LSmanager->getSurveys();

		$ans = $LSmanager->getSurveyAnswers($surveyID,'full');

		foreach ($surveys as $row)
		{
		
			 if($row['sid']==$surveyID)
			 {
			 	$title=$row['surveyls_title'];
			 }
		}


		$quest = json_decode(json_encode($ans), true);

		$empty=array();

		foreach($quest['content']['responses'] as $count=>$val)
		{

			if(key($val)==$answerID)
			{
				$empty=$val;
			}


		}
		$html='<html>
			   <head>
			   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		       <style>
		       body { font-family: DejaVu Sans, sans-serif; }
			   table, th, td {
			   font-size:9px;
    	       border: 0.3px inset black;
		}
		      </style>
			  </head>
			  <body>';
		$html.="<h4>".$title."</h4>";
		$html.=View::make('surveyTheme::backend.pdftemplate')
		->with('answers',$empty[$answerID])->render();
	

		 $string=" Podgląd Ankiety";
		
		 $string.="Dziękujemy za wypełnienie";

		 $dompdf = new Dompdf();

		 $dompdf->set_option('defaultFont', 'Courier');

		 $dompdf->loadHtml($html);

		 $dompdf->set_option("isPhpEnabled", true);

		// (Optional) Setup the paper size and orientation
		 $dompdf->setPaper('A4');

		// Render the HTML as PDF
		 $dompdf->render();

		// Output the generated PDF to Browser
		$body=$dompdf->output();

		$filename = $surveyID.'_'.$answerID."_limesurvey_data.pdf";
		file_put_contents('/epro/panel/public/uploads/'.$filename, $body);
		header("Content-type:application/pdf");
		echo json_encode(array(
			'file' => '/epro/panel/public/uploads/'.$filename,
		));
		return ;
	
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function htmlstat($surveyID)
	{
		$LSmanager = new SurveyService();

		$html = $LSmanager->getStat($surveyID, 'html');

		return View::make('surveyTheme::backend.htmlstat')
		->with('html',$html);
	}

	
	public function questionanswer($surveyID, $answerID)
	


	public function joinSurvey()
	{
		$id_survey=Input::get('id_survey');

		$token=Input::get('token');
		$first_exp=Input::get('first_exp');
		$two_exp=Input::get('two_exp');

		$query_token_check=" SELECT * FROM lime_tokens_".$id_survey.' WHERE token='.$token;
		$result_token = DB::connection('mysql2')->select($query_token_check);
		$query_update="UPDATE `02802686_lime`.`lime_tokens_$id_survey` SET `ekspert_id` = $first_exp,
		`ekspert2_id` = $two_exp WHERE `token` =$token;";
		$result_token = DB::connection('mysql2')->update($query_update);
  		Notification::success('Zapisano');

    return Redirect::route('survey.index');

	}

	

		
	public function singleSurvey($id,$userid)
	{

	  $user=ClientModel::findOrFail($userid);
	  $token=$user->pesel.$userid;
	  $survey_id=$id;
	  $survey_type=" SELECT `token`,`id` FROM `lime_survey_$id` WHERE token LIKE'%$token%'";
	  $survey_answer =DB::connection('mysql2')->select($survey_type);
	 
	  return View::make('surveyTheme::backend.singlesurvey')->with('answers',$survey_answer)->with('surveyID',$survey_id);
	}


}
