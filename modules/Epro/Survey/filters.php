<?php

App::before(function($request)
{

	View::addNamespace('surveyTheme', app('path').'/Epro/modules/Survey/views');
	Lang::addNamespace('surveyTranslate', app('path').'/Epro/modules/Survey/lang');

});