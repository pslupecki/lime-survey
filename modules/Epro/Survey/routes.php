<?php

Route::group(array('prefix' => 'survey', 'namespace' => 'App\Epro\Modules\Survey\Controller\Backend','before' => 'auth.admin'),function() {
	Route::delete('mapdelete/{id}',array('as' => 'My.route','uses' => 'SurveyController@mapremove'));

	Route::get('index', array('as'=> 'survey.index', 'uses' => 'SurveyController@index'));
	Route::get('singlesurvey/{surveyID}/{userid}', array('as'=> 'survey.singlesurvey', 'uses' => 'SurveyController@singleSurvey'));

	Route::get('stat/{sid}/{type}', array('as'=> 'survey.stat', 'uses' => 'SurveyController@stat'));
	Route::get('pdf/{surveyid}/{questionanswer}', array('as'=> 'survey.stat', 'uses' => 'SurveyController@pdf'));
	Route::get('htmlstat/{sid}', array('as'=> 'survey.htmlstat', 'uses' => 'SurveyController@htmlstat'));
	Route::get('surveyanswers/{sid}', array('as'=> 'survey.surveyanswers', 'uses' => 'SurveyController@surveyanswers'));
	Route::get('process/{sid}', array('as'=> 'survey.process', 'uses' => 'SurveyController@process'));
	Route::get('expert/{sid}/{qid}', array('as'=> 'survey.expert', 'uses' => 'SurveyController@expert'));
	Route::get('surveyfill/{sid}', array('as'=> 'survey.surveyfill', 'uses' => 'SurveyController@surveyfill'));
	Route::get('Notefill/{surveyID}/{sid}', array('as'=> 'survey.Notefill', 'uses' => 'SurveyController@noteSurveyFill'));
	Route::get('map/', array('as'=> 'survey.map', 'uses' => 'SurveyController@map'));
	Route::post('makemap/', array('as'=> 'survey.makeMap', 'uses' => 'SurveyController@makeMap'));
	Route::get('note/{sid}', array('as'=> 'survey.note', 'uses' => 'SurveyController@surveynote'));

	Route::post('expert/', array('as'=> 'survey.expert', 'uses' => 'SurveyController@joinSurvey'));

	Route::get('questionanswer/{surveyID}/{sid}', array('as'=> 'survey.questionanswer', 'uses' => 'SurveyController@questionanswer'));
	Route::get('singleanswer/{surveyID}/{sid}', array('as'=> 'survey.singleanswer', 'uses' => 'SurveyController@singleAnswer'));

});
