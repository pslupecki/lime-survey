<?php
namespace App\Epro\Modules\Survey\Models;


class SurveyAnswerRelation extends \Eloquent
{
	/**
	 * Table name
	 * @var string
	 */
	protected $table = 'survey_answer_relation';

    protected $fillable = array('survey_id','answer_id');

    public function clients()
    {
    	return $this->belongsTo('App\Epro\Modules\Client\Models\Client','client_id','survey_id');
    }


}
