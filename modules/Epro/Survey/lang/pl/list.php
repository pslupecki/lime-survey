<?php

return [
	'active' => 'Aktywność',
	'name' => "Nazwa ankiety",
	'actions' => 'Akcje',
	'answers' => 'Odpowiedzi',
	'answer_number' => 'Odpowiedź nr. ',
	'finished' => 'Ukończone',
	'question' => "Pytanie",
	'answer' => "Odpowiedź"
];