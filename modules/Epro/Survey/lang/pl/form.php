<?php 

return array(
	'load' => 'Wczytaj',
	'save' => 'Zapisz',
	'loadfile' => "Wczytaj plik",
	'header' => 'Dostępne ankiety',
	'headerAnswer' => "Dostępne odpowiedzi",
	'infotext_loadfile' => '1. Wczytaj plik xls z klientami. Nastapi zapis danych do tymczasowej tablicy.',
	'infotext_importfile' => '2. Wczytanie danych spowoduje usunięcie obecnej bazy klientów.',
	'htmlstat' => 'Ankieta'
);